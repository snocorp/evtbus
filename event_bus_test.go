package evtbus

import (
	"fmt"
	"testing"
	"time"
)

const topic = "topic"

func TestBasicFlow(t *testing.T) {
	var x int
	handle := Subscribe(topic, func(a int) {
		fmt.Printf("Received %v\n", a)
		x = a
	})
	Publish(topic, 99)

	if x != 99 {
		t.Errorf("Expected 99, but got %v", x)
	}
	handle.unsubscribe()

	Publish(topic, 100)
	if x == 100 {
		t.Errorf("Expected 99, but got %v", x)
	}
}

func TestMultiSubscribe(t *testing.T) {
	var x1, x2 int
	handle1 := Subscribe(topic, func(a int) {
		x1 = a
	})
	Publish(topic, 99)

	if x1 != 99 {
		t.Errorf("Expected 99, but got %v", x1)
	}
	handle2 := Subscribe(topic, func(a int) {
		x2 = a
	})
	Publish(topic, 98)

	if x1 != 98 {
		t.Errorf("Expected 98, but got %v", x1)
	}
	if x2 != 98 {
		t.Errorf("Expected 98, but got %v", x1)
	}
	handle1.unsubscribe()

	Publish(topic, 97)
	if x1 == 97 {
		t.Errorf("Expected 98, but got %v", x1)
	}
	if x2 != 97 {
		t.Errorf("Expected 97, but got %v", x2)
	}
	handle2.unsubscribe()

	Publish(topic, 96)
	if x1 == 96 {
		t.Errorf("Expected 98, but got %v", x1)
	}
	if x2 == 96 {
		t.Errorf("Expected 97, but got %v", x2)
	}
}

func TestMultiChannel(t *testing.T) {
	var x1, x2 int
	Subscribe(topic, func(a int) {
		x1 = a
	})
	Subscribe("other", func(a int) {
		x2 = a
	})

	Publish(topic, 99)
	Publish("other", 100)

	if x1 != 99 {
		t.Errorf("Expected 99, but got %v", x1)
	}
	if x2 != 100 {
		t.Errorf("Expected 100, but got %v", x2)
	}
}

func TestSubscribeOnce(t *testing.T) {
	var x int
	SubscribeOnce(topic, func(a int) {
		x = a
	})
	Publish(topic, 99)
	Publish(topic, 100)

	if x != 99 {
		t.Errorf("Expected 99, but got %v", x)
	}
}

func TestSubscribeAsync(t *testing.T) {
	var x int
	SubscribeAsync(topic, func(a int) {
		time.Sleep(10*time.Millisecond)
		x = a
	})

	handle := Publish(topic, 99)
	if x == 99 {
		t.Errorf("Expected 0, but got %v", x)
	}
	Wait(handle)
	if x != 99 {
		t.Errorf("Expected 99, but got %v", x)
	}
}

func TestSubscribeOnceAsync(t *testing.T) {
	var x int
	SubscribeOnceAsync(topic, func(a int) {
		time.Sleep(10*time.Millisecond)
		x = a
	})

	handle := Publish(topic, 99)
	if x == 99 {
		t.Errorf("Expected 0, but got %v", x)
	}
	Wait(handle)
	if x != 99 {
		t.Errorf("Expected 99, but got %v", x)
	}

	handle = Publish(topic, 98)
	Wait(handle)
	if x != 99 {
		t.Errorf("Expected 99, but got %v", x)
	}
}

func TestUnsubscribe(t *testing.T) {
	var x int
	handle := Subscribe(topic, func(a int) {
		x = a
	})
	Unsubscribe(handle)
	Publish(topic, 99)

	if x != 0 {
		t.Errorf("Expected 0, but got %v", x)
	}
}